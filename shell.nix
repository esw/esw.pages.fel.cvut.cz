with import <nixpkgs> {};
mkShell {
  nativeBuildInputs = [
    bashInteractive
    mkdocs
    python3Packages.markdown-include
    python3Packages.pymdown-extensions
  ];
}
