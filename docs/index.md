# B4M36ESW – Efficient Software

Within the course of [Efficient software][] you will get familiar with
the area of software and algorithm optimization under limited resources.
The course focuses on the efficient use of modern hardware
architectures – multi-core and multi-processor systems with shared
memory. Students will practically implement and use presented techniques
in C/C++/Rust and Java. Main topics are: code optimization, efficient data
structures and processor cache usage, data structures in multi-threaded
applications, and implementation of efficient network servers.

[Efficient software]: https://fel.cvut.cz/cz/education/bk/predmety/47/01/p4701906.html

## Assessment

Graded assessment is based on the point evaluation, where the final
grade is given by the evaluation scale [The study and examination code
of CTU][ctu exam code], article 15. A student can obtain 60 points for
the work during the practical part of the course and 30 points for the
written final evaluation. Oral examination is optional and a student can
obtain up to 10 points. In order to get zápočet/basic assessment (the
condition prior final examination), a student must have at least 30
points for the work during the [practical part of the course][labs] and
all tasks must be successfully submitted. A student can pass examination
if he/she has at least 20 points from final examination. The final grade
is given by the sum of points according to the evaluation scale.

[ctu exam code]: http://www.fel.cvut.cz/en/education/rules/Study_and_Exam_Code.pdf

[labs]: #labs

### Example of exam test

Time limit for both parts together is 60 minutes.

- [JAVA part test example][java test]
- [C/C++ part test example][c test]

[java test]: pdfs/example_exam_test_java.pdf

[c test]: pdfs/zkouska_2018-09-06en.pdf

## Lectures recordings

Recordings from 2024 are available in the [YouTube playlist](https://www.youtube.com/playlist?list=PLQL6z4JeTTQmAJev6kwllcthWxtGY_EqB).

## Lectures

| #   | Date&nbsp;&nbsp; | Topics                                                                                                                                                                                                                    | Slides                                                                                                        |
|-----|-----------------:|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1.  |           17. 2. | Introduction, modern computer architecture, C compilers.                                                                                                                                                                  | [slides](pdfs/esw01-intro.pdf)                                                                                |
| 2.  |           24. 2. | Bentley's rules, C compiler, profiling.                                                                                                                                                                                   | [slides 1](pdfs/esw02-bentley.pdf), [slides 2](pdfs/esw02-compiler.pdf), [code](assets/esw02_2017_src.tar.gz) |
| 3.  |            3. 3. | Benchmarking, measurements, metrics, statistics, WCET, timestamping.                                                                                                                                                      | [slides](pdfs/esw03-benchmarking.pdf)                                                                         |
| 4.  |           10. 3. | Scalable synchronization – from mutexes to RCU (read-copy-update), transactional memory, scalable API.                                                                                                                    | [slides](pdfs/esw04-synchronization.pdf)                                                                      |
| 5.  |           17. 3. | Memory access – efficient programming with caches, dynamic memory allocation (malloc, NUMA, ...).                                                                                                                         | [slides](pdfs/esw05-memory-access.pdf), [animations](assets/esw08-animations.tar.gz)                          |
| 6.  |           24. 3. | Serialization of data structures – JSON, XML, protobufs, AVRO, cap'n'proto, mmap/shared memory.                                                                                                                           | [slides](pdfs/esw06-serialization.pdf)                                                                        |
| 7.  |           31. 3. | Program run – virtual machine, byte-code, Java compiler, JIT compiler, relation to machine code, byte-code analysis, dissasembly of Java byte-code, optimization in compilers, program performance analysis, profiling.   | [slides](pdfs/ESW07_2024.pdf)                                                                                 |
| 8.  |           7. 4. | Data concurrency in JVM – multi-threaded access to data, locks monitoring, atomic operations, lock-less/block-free data structures, non-blocking algorithms (queue, stack, set, dictionary), data races, synchronization. | [slides](pdfs/ESW08_2024.pdf)                                                                                 |
| 9.  |           14. 4. | Efficient servers, C10K, non-blocking I/O, efficient networking, threads                                                                                                                                                  | [slides](pdfs/ESW09_2024.pdf)                                                                                 |
|     |           21. 4. | Easter                                                                                                                                                                                                                    |                                                                                                               |
| 10. |           28. 4. | JVM – Memory analysis (dynamic/static), data structures, collections for performance.                                                                                                                                     | [slides](pdfs/ESW10_2024.pdf)                                                                                 |
| 11. |            5. 5. | JVM – Object allocation, bloom filters, references, effective caching.                                                                                                                                                    | [slides](pdfs/ESW11_2024.pdf)                                                                                 |
| 12. |           12. 5. | Virtualization (IOMMU, SR-IOV, PCI pass-through, virtio, …).                                                                                                                                                              | [slides](pdfs/esw13-virtualization.pdf), [code](assets/vmm.tar.gz)                                            |
| 13. |           19. 5. | Memory Management in JVM – Memory Layout, Garbage Collectors.                                                                                                                                                             | [slides](pdfs/ESW13_2024.pdf)                                                                                 |

## Labs

You will be given nine tasks during the semester. Eight simpler tasks
are scored by 5 points per task. The ninth task is more complex and is
scored by 20 points, including functionality and code quality. The order
of the tasks and deadlines may change before their assignment.

| Date   | Task assignment                 | Language   | Max. points | Deadline |
|--------|---------------------------------|------------|-------------|----------|
| 17. 2. | [How to compile C project][]    | C/C++      |             |          |
| 24. 2. | T1: [Profiling C][]             | C/C++      | 5           |          |
| 3. 3.  | T2: [Asynchronous I/O][]        | C/C++      | 5           |          |
| 10. 3. | T3: [Read Copy Update][]        | C/C++      | 5           | T1       |
| 17. 3. | T4: [Benchmarking][]            | Java       | 5           | T2       |
| 24. 3. | T5: [Serialization][]           | Java/C/C++ | 5           | T3       |
| 31. 3. | T6: [Profiling Java][]          | Java       | 5           | T4       |
| 7. 4.  | T7: [Non-blocking Algorithms][] | Java       | 5           | T5       |
| 14. 4. | T8: [Efficient servers][]       | whatever   | 20          | T6       |
| 21. 4. | Easter                          |            |             |          |
| 28. 4. | T9: [Vector API (Java)][]       | Java       | 5           | T7       |
| 5. 5.  |                                 |            |             |          |
| 12. 5. |                                 |            |             | T9       |
| 19. 5. |                                 |            |             | T8       |
|        | **Total**                       |            | **60 (+8)** |          |

[How to compile C project]: labs/how-to-compile-c.md
[Profiling C]: labs/profiling-c.md
[Benchmarking]: labs/benchmarking.md
[Read Copy Update]: labs/rcu.md
[Asynchronous I/O]: labs/asynchronous-io.md
[Serialization]: labs/serialization.md
[Profiling Java]: labs/profiling-java.md
[Non-blocking Algorithms]: labs/non-blocking-algorithms.md
[Efficient servers]: labs/efficient-servers.md
[Vector API (Java)]: labs/java-vector-api.md

Tasks are submitted into the upload system ([BRUTE][]) and in some
cases also to your tutor. The maximum amount of points will be given
when the task is uploaded in time, otherwise, penalty points will be
applied – one point per week for simpler tasks and two points per week
for the complex task will be deducted from the total task score.
Simpler tasks submitted after solution presentation (4-5 weeks after
the assignment) will be rewarded by 0 points (**you still have to submit
it**).

All the tasks have to be submitted before the start of the examination
period.

Lab attendance is recorded (voluntary). However, the order of
consultations and submissions during labs is based on your attendance.

If you have any questions, please ask via [GitLab Issues][].

In order to obtain zápočet/basic assessment (the condition prior to
the final examination), all tasks must be submitted with a sum of
**at least 30 points**.

[GitLab Issues]: https://gitlab.fel.cvut.cz/esw/esw.pages.fel.cvut.cz/-/issues
[BRUTE]: https://cw.felk.cvut.cz/brute/student/course/ESW

## Contacts

- [GitLab Issues (preferred)][gitlab issues]
- Lecturers:
    - [Ing. Michal Sojka, Ph.D.](mailto:Michal.Sojka@cvut.cz)
    - [doc. Ing. David Šišlák, Ph.D.](mailto:sislakd@fel.cvut.cz)
- Practising:
    - [Ing. Michal Sojka, Ph.D. (C/C++)](mailto:Michal.Sojka@cvut.cz)
    - [Ing. Tomáš Hauser (Java)](mailto:tomas.hauser1@gmail.com)

## Literature

- MIT: Performance-engineering-of-software-systems
- Oaks, S.: Java Performance: 2nd Edition. O'Reilly, USA 2020.
- Jones, R., Hosking, A., Moss, E.: The Garbage Collection Handbook –
  The Art of Automatic Memory Management. CRC Press, USA 2012.
- Herlihy, M., Shavit, N.: The Art of Multiprocessor Programming. Morgan
  Kaufman, 2008.
- Fog, A.: The microarchitecture of Intel, AMD and VIA CPU, 2016.
  ([online](http://www.agner.org/optimize/microarchitecture.pdf))
- Drepper U.: What every programmer should know about memory, 2007
- Jain, R.: The Art of Computer Systems Performance Evaluation. Wiley,
  New York 1991. (slides, book)
- Lilja, D. J.: Measuring Computer Performance: A Practitioner's Guide.
  Cambridge University Press, 2000. (book web site, Supplemental
  Teaching Materials)
